//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다. 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FALSE 0
#define TRUE 1

typedef struct
{
	char name[50];
	int age;
	int salary;
}humanBeing;

int humansEqual(humanBeing*, humanBeing*);

int main()
{
	humanBeing person[2];
	int i;

	/* 구조체 입력 2번 */
	for (i = 0; i < 2; i++) {
		printf("Input person%d's name, age, salary : \n", i + 1);
		gets(person[i].name);
		scanf("%d", &person[i].age);
		scanf("%d", &person[i].salary);
		fflush(stdin);
	}

	if (humansEqual(&person[0], &person[1]) == TRUE)
		printf("The two human beings are the same \n");
	else
		printf("The two human beings are not the same \n");
	return 0;
}

int humansEqual(humanBeing *person1, humanBeing *person2)
{
	if (strcmp(person1->name, person2->name))
		return FALSE;
	if (person1->age != person2->age)
		return FALSE;
	if (person1->salary != person2->salary)
		return FALSE;
	return TRUE;
}