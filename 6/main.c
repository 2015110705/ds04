//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다. 

#include <stdio.h>
#include <stdlib.h>

#define MAX_TERMS 100 /* size of terms array */
#define TRUE 1
#define FALSE 0
#define COMPARE(X, Y) (X < Y ? -1 : X == Y ? 0 : 1)

typedef struct {
	float coef;
	int expon;
} term;

term terms[MAX_TERMS];
int avail = 0;

void padd(int, int, int, int, int*, int*);
void attach(float, int);
void printPoly(char, int, int);

int main()
{
	int nSizeA, nSizeB;
	int i;
	int startD, finishD;
	FILE* pInput = fopen("input.txt", "r");

	printf("<< D(x) = A(x) + B(x) >> \n");

	/* term A 배열 개수 */
	fscanf(pInput, "%d", &nSizeA);
	avail += nSizeA;

	/* term B 배열 개수 */
	fscanf(pInput, "%d", &nSizeB);
	avail += nSizeB;

	/* 입력 */
	for (i = 0; i < nSizeA; i++) {
		fscanf(pInput, "%f %d", &terms[i].coef, &terms[i].expon);
	}
	printf("\n");

	for (i = nSizeA; i < avail; i++) {
		fscanf(pInput, "%f %d", &terms[i].coef, &terms[i].expon);
	}
	printf("\n");

	/* 덧셈 연산 */
	padd(0, nSizeA, nSizeA, nSizeA + nSizeB, &startD, &finishD);

	/* 답 출력 */
	printPoly('A', 0, nSizeA);
	printPoly('B', nSizeA, nSizeA + nSizeB);
	printPoly('D', startD, finishD - 1);

	return 0;
}

void padd(int startA, int startB, int finishA, int finishB, int* startD, int* finishD)
{
	float coefficient;
	*startD = avail;
	while (startA <= finishA && startB <= finishB)
		switch (COMPARE(terms[startA].expon, terms[startB].expon)) {
		case -1:
			attach(terms[startB].coef, terms[startB].expon);
			startB++;
			break;
		case 0:
			coefficient = terms[startA].coef + terms[startB].coef;
			if (coefficient)
				attach(coefficient, terms[startA].expon);
			startA++;
			startB++;
		case 1:
			attach(terms[startA].coef, terms[startA].expon);
			startA++;
	}

	for (; startA <= finishA; startA++)
		attach(terms[startA].coef, terms[startA].expon);

	for (; startB <= finishB; startB++)
		attach(terms[startB].coef, terms[startB].expon);
	*finishD = avail - 1;
}

void attach(float coefficient, int exponent)
{
	if (avail >= MAX_TERMS) {
		fprintf(stderr, "Too many terms in the polynomail \n");
		exit(EXIT_FAILURE);
	}
	terms[avail].coef = coefficient;
	terms[avail++].expon = exponent;
}

void printPoly(char name, int start, int finish)
{
	int i = start;

	printf("%c(x) = %f", name, terms[i].coef);
	if (terms[i].expon != 0)
		printf("x^%d ", terms[i].expon);

	for (i = start + 1; i < finish; i++) {
		if (terms[i].coef < 0)
			printf("- ");
		else printf("+ ");
		printf("%f", terms[i].coef);
		if (terms[i].expon != 0)
			printf("x^%d ", terms[i].expon);
	}

	printf("\n");
}