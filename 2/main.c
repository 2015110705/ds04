//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

int sumAry3D_f1(int[2][3][4], int, int, int);
int sumAry3D_f2(int[][3][4], int, int, int);
int sumAry3D_f3(int(*)[3][4], int, int, int);
int sumAry3D_f4(int***, int, int, int);
int sumAry3D_f5(int****, int, int, int);
void freeAry3D(int***, int, int);

int main()
{
	int ary2D[][3][4] = {
		{
			{ 1, 2, 3, 4 },
			{ 5, 6, 7, 8 },
			{ 9, 10, 11, 12 }
		},
		{
			{ 13, 14, 15, 16 },
			{ 17, 18, 19, 20 },
			{ 21, 22, 23, 24 }
		}
	};

	int i, j, k;
	int ***ary = (int***)malloc(sizeof(int**) * 2);
	for (i = 0; i < 2; i++) {
		ary[i] = (int**)malloc(sizeof(int*) * 3);

		for (j = 0; j < 3; j++) {
			ary[i][j] = (int*)malloc(sizeof(int) * 4);
		}
	}

	for (i = 0; i < 2; i++)
		for (j = 0; j < 3; j++)
			for (k = 0; k < 4; k++)
				ary[i][j][k] = i + j + k;

	printf("sumAry2D_f1() %d \n", sumAry3D_f1(ary2D, 2, 3, 4));
	printf("sumAry2D_f2() %d \n", sumAry3D_f2(ary2D, 2, 3, 4));
	printf("sumAry2D_f3() %d \n", sumAry3D_f3(ary2D, 2, 3, 4));

	printf("sumAry2D_f4() %d \n", sumAry3D_f4(ary, 2, 3, 4));
	printf("sumAry2D_f5() %d \n", sumAry3D_f5(&ary, 2, 3, 4));

	freeAry3D(ary, 2, 3);

	return 0;
}

int sumAry3D_f1(int ary[2][3][4], int dims, int rows, int cols)
{
	int i, j, k;
	int total = 0;

	for (i = 0; i < dims; i++)
		for (j = 0; j < rows; j++)
			for (k = 0; k < cols; k++)
				total += ary[i][j][k];

	return total;
}

int sumAry3D_f2(int ary[][3][4], int dims, int rows, int cols)
{
	int i, j, k;
	int total = 0;

	for (i = 0; i < dims; i++)
		for (j = 0; j < rows; j++)
			for (k = 0; k < cols; k++)
				total += ary[i][j][k];

	return total;
}

int sumAry3D_f3(int(*ary)[3][4], int dims, int rows, int cols)
{
	int i, j, k;
	int total = 0;

	for (i = 0; i < dims; i++)
		for (j = 0; j < rows; j++)
			for (k = 0; k < cols; k++)
				total += ary[i][j][k];

	return total;
}

int sumAry3D_f4(int ***ary, int dims, int rows, int cols)
{
	int i, j, k;
	int total = 0;

	for (i = 0; i < dims; i++)
		for (j = 0; j < rows; j++)
			for (k = 0; k < cols; k++)
				total += ary[i][j][k];

	return total;
}

int sumAry3D_f5(int****ary, int dims, int rows, int cols)
{
	int i, j, k;
	int total = 0;

	for (i = 0; i < dims; i++)
		for (j = 0; j < rows; j++)
			for (k = 0; k < cols; k++)
				total += (*ary)[i][j][k];

	return total;
}

void freeAry3D(int*** ary, int dim, int rows)
{
	int i, j;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < rows; j++) {
			free(ary[i][j]);
		}
		free(ary[i]);
	}

	free(ary);
}