//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

void add(int **, int **, int **, int, int);
int** make2dArray(int, int);
void free2dArray(int**, int);void init2dArray(int**, int, int);
void print2dArray(int**, int, int);

int main()
{
	int nInputCols, nInputRows;
	int** anArrayA;
	int** anArrayB;
	int** anArrayC;

	printf("Enter row size and column size (ex) 3 4 : ");
	scanf("%d %d", &nInputRows, &nInputCols);

	/* 배열 동적할당 */
	anArrayA = make2dArray(nInputRows, nInputCols);
	anArrayB = make2dArray(nInputRows, nInputCols);
	anArrayC = make2dArray(nInputRows, nInputCols);

	/* A, B 배열 값 입력 */
	printf("[matrix A] \n");
	init2dArray(anArrayA, nInputRows, nInputCols);

	printf("[matrix B] \n");
	init2dArray(anArrayB, nInputRows, nInputCols);

	/* 행렬 덧셈 연산 */
	add(anArrayA, anArrayB, anArrayC, nInputRows, nInputCols);

	/* 행렬 덧셈 결과 C 배열 출력 */
	printf("[matrix C] \n");
	print2dArray(anArrayC, nInputRows, nInputCols);

	/* 동적할당 해제 */
	free2dArray(anArrayA, nInputRows);
	free2dArray(anArrayB, nInputRows);
	free2dArray(anArrayC, nInputRows);
	printf("2d array - free!! \n");

	return 0;
}

void add(int **a, int **b, int **c, int rows, int cols)
{
	int i, j;

	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			c[i][j] = a[i][j] + b[i][j];
}

int** make2dArray(int rows, int cols)
{
	int **x, i;

	x = (int**)malloc(sizeof(int*) * rows);

	for (i = 0; i < rows; i++)
		x[i] = (int*)malloc(sizeof(int) * cols);

	return x;
}

void free2dArray(int** ary, int rows)
{
	int i, j;

	for (i = 0; i < rows; i++)
		free(ary[i]);

	free(ary);
}

void init2dArray(int **ary, int rows, int cols)
{
	int i, j;

	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			scanf("%d", &ary[i][j]);
}

void print2dArray(int **ary, int rows, int cols)
{
	int i, j;

	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++) {
			printf("%d ", ary[i][j]);
		}
		printf("\n");
	}
}