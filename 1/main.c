//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

int sumAry2D_f1(int[2][3], int, int);
int sumAry2D_f2(int[][3], int, int);
int sumAry2D_f3(int(*)[3], int, int);
int sumAry2D_f4(int**, int, int);
int sumAry2D_f5(int***, int, int);
void freeAry2D(int**, int);

int main()
{
	int ary2D[][3] = {
		{ 1, 2, 3 },
		{ 4, 5, 6 }
	};

	int r, c;
	int **ary = (int**)malloc(sizeof(int*) * 2);
	for (r = 0; r < 2; r++)
		ary[r] = (int*)malloc(sizeof(int) * 3);

	for (r = 0; r < 2; r++)
		for (c = 0; c < 3; c++)
			ary[r][c] = r + c;

	printf("sumAry2D_f1() %d \n", sumAry2D_f1(ary2D, 2, 3));
	printf("sumAry2D_f2() %d \n", sumAry2D_f2(ary2D, 2, 3));
	printf("sumAry2D_f3() %d \n", sumAry2D_f3(ary2D, 2, 3));

	printf("sumAry2D_f4() %d \n", sumAry2D_f4(ary, 2, 3));
	printf("sumAry2D_f5() %d \n", sumAry2D_f5(&ary, 2, 3));

	freeAry2D(ary, 2);

	return 0;
}

int sumAry2D_f1(int ary[2][3], int rows, int cols)
{
	int i, j;
	int total = 0;

	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			total += ary[i][j];

	return total;
}

int sumAry2D_f2(int ary[][3], int rows, int cols)
{
	int i, j;
	int total = 0;

	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			total += ary[i][j];

	return total;
}
int sumAry2D_f3(int(*ary)[3], int rows, int cols)
{
	int i, j;
	int total = 0;

	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			total += ary[i][j];

	return total;
}

int sumAry2D_f4(int **ary, int rows, int cols)
{
	int i, j;
	int total = 0;

	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			total += ary[i][j];

	return total;
}
int sumAry2D_f5(int ***ary, int rows, int cols)
{
	int i, j;
	int total = 0;

	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			total += (*ary)[i][j];

	return total;
}

void freeAry2D(int** ary, int rows)
{
	int i, j;

	for (i = 0; i < rows; i++)
		free(ary[i]);

	free(ary);
}